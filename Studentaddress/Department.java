package Studentaddress;
class Department {
		int deptId;
		String deptName;
		int managerid;
		
	public Department(int deptId, String deptName, int managerid) {
			super();
			this.deptId = deptId;
			this.deptName = deptName;
			this.managerid = managerid;
		}

	public static void main(String[] args)
	
	{
		Employee e1 = new Employee(1,"Bevi",100000,new Department(121,"ECE",12));
		Employee e2 = new Employee(2,"viji",50000,new Department(213,"ECE",21));
		Employee e3 = new Employee(3,"vasu",80000,new Department(234,"ECE",13));
		Employee e4 = new Employee(4,"vini",410000,new Department(241,"ECE",31));
		Employee e5 = new Employee(5,"vincy",60000,new Department(541,"ECE",41));
		System.out.println(e1);
		System.out.println(e2);
		e1.dispDepartment();
		

	}

}
