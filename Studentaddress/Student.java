package Studentaddress;

public class Student {
	private int studId;
	private String studName;
	private Address addr;
	public Student(int studId, String studName, Address addr) {
		super();
		this.studId = studId;
		this.studName = studName;
		this.addr = addr;
	}
		
	@Override
	public String toString() {
		return "Student [studId=" + studId + ", studName=" + studName + "]";
	}
	}
	class Address
	{
		int stNo;
		String streetname;
		String city;
		public Address(int stNo, String streetname, String city) {
			super();
			this.stNo = stNo;
			this.streetname = streetname;
			this.city = city;
		}
		
	}
