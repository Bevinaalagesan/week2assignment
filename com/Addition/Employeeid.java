
	package com.Addition;

	public class Employeeid {
		 public int empId;
		 public String empName;
		 public float empSalary;
		public Employeeid()//without
		{
			empId = 100;
			empName = "Tina";
			empSalary = 56565.50f;
		}
		//with argu
		public Employeeid(int empId, String empName, float empSalary) {
			//this - current class 
			
			this.empId = empId;
			this.empName = empName;
			this.empSalary = empSalary;
		}
		
		
	}

