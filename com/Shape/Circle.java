package com.Shape;

public class Circle extends Shapes{
	public int radius;

	public Circle(String name, float area, int radius) {
		super(name, area);
		this.radius = radius;
	}
	public float calcArea()
	{
		return (float)(3.14*radius*radius);
	}
	
	}