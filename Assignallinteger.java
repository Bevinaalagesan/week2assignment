import java.util.Scanner;

public class Assignallinteger {

	public static void main(String[] args) {
		
		        Scanner s1 = new Scanner(System.in);
		        System.out.print("Input 1st integer: ");
		        int firstnum = s1.nextInt();
		        System.out.print("Input 2nd integer: ");
		        int secondnum = s1.nextInt();

		        System.out.printf("Sum of two integers: %d%n", firstnum + secondnum);
		        
		        System.out.printf("Difference of two integers: %d%n", firstnum - secondnum);
		        
		        System.out.printf("Product of two integers: %d%n", firstnum * secondnum);
		        
		        System.out.printf("Average of two integers: %.2f%n", (double) (firstnum + secondnum) / 2);
		        
		        System.out.printf("Distance of two integers: %d%n", Math.abs(firstnum - secondnum));
		        
		        System.out.printf("Max integer: %d%n", Math.max(firstnum, secondnum));
		        
		        System.out.printf("Min integer: %d%n", Math.min(firstnum, secondnum));
		    }
		

	}


